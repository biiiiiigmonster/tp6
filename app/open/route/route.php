<?php
use think\facade\Route;

Route::miss('Error/miss');
Route::get('/swagger',function (){
    $openapi = \OpenApi\scan(base_path());
    header('Content-Type: application/x-yaml');
    $jsonStr = $openapi->toJson();
    echo $jsonStr;
});
Route::get('/','Index/index');
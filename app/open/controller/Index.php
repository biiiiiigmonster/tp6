<?php

namespace app\open\controller;

use app\BaseController;
use app\open\model\Order;
use think\facade\Cache;
use think\facade\Queue;

class Index extends BaseController
{
    /**
     * 首页
     * @return \think\response\Json
     * @throws \Exception
     */
    public function index()
    {
        $data = $this->request->param();

        $ret = rpc_request('tcp://123.207.255.238:18307', \App\Rpc\Lib\UserInterface::class, 'getList',  [1, 2], "1.0");

        return json('好开心哦');
    }
}

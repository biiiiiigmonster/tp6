<?php

namespace app\event;

use app\auth\model\User;

class UserLogin
{
    public $user;

    public function __construct(User $user)
    {
        echo '[event]';
        $this->user = $user;
    }
}

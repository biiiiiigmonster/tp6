<?php


namespace app\http\middleware\web;


use Closure;

class Session
{
    /**
     * 登陆成功之后，将用户凭证保存至session
     * 后置中间件，如需触发，则需要在控制器中间件中指定即可
     * 此处的流程应该与api流程一致，即生成
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $returnData = $response->getData();


        return $response;
    }
}
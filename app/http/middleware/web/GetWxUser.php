<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/3/14
 * Time: 17:43
 */

namespace app\http\middleware\web;

use app\index\model\User;
use Closure;
use think\facade\Session;

class GetWxUser
{
    /**
     * * 1、获取微信用户信息，判断有没有code，有使用code换取access_token，没有去获取code。
     * @param $request
     * @param Closure $next
     * @return array 微信用户信息数组
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('wxUser')) {
            if (!isset($_GET['code'])) {//没有code，去微信接口获取code码
                $callback = $request->url(true);//微信服务器回调url，这里是当前页url
                $this->get_code($callback);
            } else {//获取code后跳转回来到这里了
                $code = $_GET['code'];
                $data = $this->get_access_token($code);//获取网页授权access_token和用户openid
                //获取了openid之后直接默认登录
                $res = User::where(['openid'=>$data['openid']])->find();
                if($res) {
                    Session::set('loginInfo',$res->toArray());
                }
                //获取微信用户信息
                $data_all = $this->get_user_info($data['access_token'], $data['openid']);
                Session::set('wxUser', $data_all);
            }
        }
        return $next($request);
    }

    /**
     * 2、用户授权并获取code
     * @param string $callback 微信服务器回调链接url
     */
     private function get_code($callback){
         $appid = WXAPPID;
         $scope = 'snsapi_userinfo';
         $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $appid . '&redirect_uri=' . urlencode($callback) .  '&response_type=code&scope=' . $scope . '&state=STATE#wechat_redirect';
         header("Location:$url");exit;
     }
    /**
     * 3、使用code换取access_token
     * @param string 用于换取access_token的code，微信提供
     * @return array access_token和用户openid数组
     */
     private function get_access_token($code){
         $appid = WXAPPID;
         $appsecret = WXAPPSECRET;
         $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $appid . '&secret=' . $appsecret . '&code=' . $code . '&grant_type=authorization_code';
         $user = json_decode(file_get_contents($url));
         if (isset($user->errcode)) {
             echo 'error:' . $user->errcode.'<hr>msg  :' . $user->errmsg;exit;
         }
         $data = json_decode(json_encode($user),true);//返回的json数组转换成array数组
         return $data;
     }
    /**
     * 4、使用access_token获取用户信息
     * @param string access_token
     * @param string 用户的openid
     * @return array 用户信息数组
     */
     private function get_user_info($access_token,$openid){
         $url = 'https://api.weixin.qq.com/sns/userinfo?access_token=' . $access_token . '&openid=' . $openid . '&lang=zh_CN';
         $user = json_decode(file_get_contents($url));
         if (isset($user->errcode)) {
             echo 'error:' . $user->errcode.'<hr>msg  :' . $user->errmsg;exit;
         }
         $data = json_decode(json_encode($user),true);//返回的json数组转换成array数组
         return $data;
     }
}
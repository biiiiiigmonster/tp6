<?php


namespace app\http\middleware\web;

use Closure;
use think\facade\Session;

class Check
{
    public function handle($request, Closure $next)
    {
        $authorization = Session::get('Authorization');
        if($authorization) {
            $request->authorizion = (object)$authorization;
        } else {
            return '';
        }

        return $next($request);
    }
}
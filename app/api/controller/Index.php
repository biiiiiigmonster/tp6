<?php

namespace app\api\controller;

use app\api\biz\IndexBiz;
use app\api\model\logic\OrderLogic;
use app\api\model\Order;
use app\BaseController;

class Index extends BaseController
{
    /**
     * @var OrderLogic
     */
    private $orderLogic;

    protected function initialize()
    {
        $this->orderLogic = new OrderLogic();
    }

    /**
     * 首页
     * @OA\Get(
     *     path="/",
     *     @OA\Response(response="200", description="An example resource")
     * )
     *
     * @return \think\response\Json
     * @throws \Exception
     */
    public function index()
    {
        $data = $this->request->param();

        $result = $this->orderLogic->index($data);

        return json($result);
    }
}
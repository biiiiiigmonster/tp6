<?php
use think\facade\Route;
use app\api\validate\Index;

Route::miss('Error/miss');
Route::get('/','Index/index')->validate(Index::class,'index');
Route::get('/explain','Index/explain');

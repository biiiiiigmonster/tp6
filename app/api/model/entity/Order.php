<?php

namespace app\api\model\entity;

use think\Model;

/**
 * Class Order
 * @package app\api\model
 */
class Order extends Model
{
    protected $connection = 'mysql1';
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

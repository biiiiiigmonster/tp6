<?php

namespace app\api\model\entity;

use think\Model;

/**
 * Class User
 * @package app\api\model\entity
 */
class User extends Model
{
    protected $hidden = ['password','delete_time','login_code'];
}

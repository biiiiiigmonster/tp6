<?php
namespace app\api\model\logic;

use app\api\model\entity\Order;

class OrderLogic
{
    public function index(array $param): array
    {
        return Order::with('user')->limit(10)->select()->toArray();
    }
}
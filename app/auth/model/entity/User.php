<?php
namespace app\auth\model\entity;

use think\Model;

/**
 * Class User
 * @package app\auth\model\entity
 */
class User extends Model
{
    protected $hidden = ['password','delete_time','login_code'];
}
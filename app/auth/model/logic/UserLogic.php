<?php
namespace app\auth\model\logic;

use app\auth\model\entity\User;
use app\common\exception\BizException;
use think\helper\Str;

class UserLogic
{
    /**
     * @param array $param
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function login(array $param): array
    {
        $user = User::where('mobile',$param['mobile'])->findOrFail();
        if(md5($param['password'])!=$user->password)
            throw new BizException('',LOGIN_FAILED);
        $user->login_status = 1;
        $user->login_code = Str::random();

        return $user->toArray();
    }

    /**
     * @param array $param
     * @return array
     */
    public function register(array $param): array
    {
        $user = User::create([
            'mobile' => $param['mobile'],
            'password' => md5($param['password']),//等待tp的bcrypt加密
        ],['mobile','password']);
        if(!$user->id)//注册失败
            throw new BizException('',FAILED);
        $user->login_status = 1;
        $user->login_code = Str::random();

        return $user->toArray();
    }
}
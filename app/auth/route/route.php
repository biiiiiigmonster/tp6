<?php
use think\facade\Route;
use app\auth\validate\User;

Route::miss('Error/miss');
Route::post('/login','Index/login')->validate(User::class,'login');
Route::post('/register','Index/register')->validate(User::class,'register');
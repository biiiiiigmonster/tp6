<?php

namespace app\auth\controller;

use think\exception\RouteNotFoundException;
use think\facade\Log;
use think\Request;

class Error
{
    /**
     * 路由解析失败
     * @throws \Exception
     */
    public function miss()
    {
        throw new RouteNotFoundException();
    }
}

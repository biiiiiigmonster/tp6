<?php

namespace app\auth\controller;

use app\auth\biz\UserBiz;
use app\auth\model\logic\UserLogic;
use app\BaseController;

class Index extends BaseController
{
    protected $middleware = [
//        'Authorize' => ['only' => ['login', 'register']],
    ];

    /**
     * @var UserLogic
     */
    private $userLogic;

    protected function initialize()
    {
        $this->userLogic = new UserLogic();
    }

    public function login()
    {
        $data = $this->request->param();

        $user = $this->userLogic->login($data);
        //事件触发
        event('UserLogin',$user);
//        event('PointsChange',$user);
        return json($user)->header($this->userService->authorize($user));//这种方式授权签名比中间件形式要来的强点
    }

    public function register()
    {
        $data = $this->request->param();

        $user = $this->userLogic->register($data);
        //事件触发
        event('UserRegister',$user);
        event('UserLogin',$user);//一般某次请求行为可以触发多种事件，比如用户注册完成时，可以触发用户登陆事件

        return json($user)->header($this->userService->authorize($user));
    }
}

<?php

namespace app\auth\validate;

use think\Validate;

class User extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
        'mobile' => 'require|mobile',
        'password' => 'require|length:8,16|confirm',
        'password_confirm'=>'require',
	];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.number' => 'id必须是数字',
    ];

    /**
     * Login场景验证
     * @return User
     */
    public function sceneLogin()
    {
        return $this->only(['mobile','password'])
            ->remove('password','confirm');
    }

    /**
     * Register场景验证
     * @return User
     */
    public function sceneRegister()
    {
        return $this->append('mobile','unique:user');
    }

    /**
     * Index场景验证
     * @return User
     */
    public function sceneIndex()
    {
        //这种用法表示当前场景不检测任何参数
        return $this->only(['']);
    }

    /**
     * Save场景验证
     * @return User
     */
    public function sceneSave()
    {
        //这种用法表示当前场景不检测任何参数
        return $this->only(['']);
    }

    /**
     * Read场景验证
     * @return User
     */
    public function sceneRead()
    {
        return $this->only(['id']);
    }

    /**
     * Update场景验证
     * @return User
     */
    public function sceneUpdate()
    {
        return $this->only(['id']);
    }

    /**
     * Delete场景验证
     * @return User
     */
    public function sceneDelete()
    {
        return $this->only(['id']);
    }
}
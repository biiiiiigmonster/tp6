<?php
declare (strict_types = 1);

namespace app\auth\util;

use Firebase\JWT\JWT;
use think\facade\Env;

class User
{
    /**
     * 授权调用
     * @param $user
     * @return array
     */
    public function authorize($user)
    {
        $time = time();
        $token = [
            'iat' => $time,//签发时间
            'nbf' => $time,//生效时间，比如设置time+30，表示当前时间30秒后才能使用
            'exp' => $time + 3600*24*30,//过期时间
            "iss" => request()->host(),//签发者
            'aud' => '*.'.request()->rootDomain(),//接收者
            'data' => [//自定义信息，不要定义敏感信息
                'id' => $user['id'],//例如用户主键id
//                'mobile' => $user['mobile'],//用户手机号
                //等等...
            ],
        ];

        $jwt = JWT::encode($token, Env::get('secret.jwt','BMW'));
        return ['Authorization' => "Bearer $jwt"];
    }
}

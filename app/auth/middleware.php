<?php
/**
 * 这是系统自动生成的auth应用middleware定义文件
 * 中间件的摆放顺序一定要斟酌
 * 应用配置文件中的中间件注册优先级高于路由定义，因此无论路由验证结果如何，这里注册到的中间件都会被执行
 */
return [
//    'Authenticate',
//    ['Throttle',['60,1']],
    'Launcher',
];

<?php
// 这是系统自动生成的auth应用event定义文件
return [
    //暂时不知道这个是干嘛用的
    'bind'      => [
//        'UserLogin' => 'app\event\UserLogin',
    ],

    //单体注册
    'listen'    => [
        'UserLogin'    =>    [
            'app\listener\UserLogin',
        ],
    ],

    //群体注册
    'subscribe' => [
        'app\subscribe\User',//用户系统订阅
    ],
];

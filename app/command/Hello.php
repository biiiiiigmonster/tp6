<?php

namespace app\command;

use app\api\model\Order;
use Faker\Factory;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Hello extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('hello')
            ->addOption('mode',null,Option::VALUE_REQUIRED,'full or cursor')
            ->setDescription('Say Hello');
    }

    protected function execute(Input $input, Output $output)
    {
        $mode = $input->getOption('mode');
        $res = $this->$mode();
    	// 指令输出
    	$output->writeln($res);
    }

    public function full()
    {
        Order::where('status','>',1)->chunk(100, function ($orders) {
            $faker = Factory::create('zh_CN');
            foreach($orders as $order){
                // 处理user模型对象
                $order->order_amount = $faker->randomFloat(2,0,1000);
                $order->discount_amount = $faker->randomFloat(2,0,$order->order_amount);
                $order->final_amount = bcsub($order->order_amount,$order->discount_amount,2);
                $order->save();
            }
        });
    }

    public function cursor()
    {

    }
}

<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/3/5
 * Time: 14:49
 */

namespace app\common\command;


use think\console\command\Make;
use think\facade\App;
use think\facade\Config;

class Validate extends Make
{
    protected $type = "Validate";

    protected function configure()
    {
        parent::configure();
        $this->setName('make:newValidate')
            ->setDescription('Create a new resource validate class');
    }

    protected function getStub()
    {
        $stubPath = __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR;

        return $stubPath . 'validate.stub';
    }

    protected function getClassName(string $name):string
    {
        return parent::getClassName($name);
    }

    protected function getNamespace(string $appNamespace):string
    {
        return parent::getNamespace($appNamespace) . '\validate';
    }
}
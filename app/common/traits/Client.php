<?php


namespace app\common\traits;

/**
 * 客户端特征收录
 * Trait Client
 * @package app\common\traits
 */
trait Client
{
    /**
     * 获取设备编码
     * @return string
     */
    public function getTerminalCode():string
    {
        //从请求头中获取当前设备唯一标识
        $terminalCode = request()->header('Terminal-Code');

        return $terminalCode?:'';
    }

    /**
     * 获取设备编码
     * @return string
     */
    public function getTerminalType():string
    {
        //从请求头中获取当前设备类型
        $terminalType = request()->header('Terminal-Type');

        return $terminalType?:'';
    }

    /**
     * 获取App环境来源
     * @return string
     */
    public function getInApp():string
    {
        return '';
    }
}
<?php
declare (strict_types = 1);

namespace app\service;

use app\auth\util\User;

class UserService extends \think\Service
{

    /**
     * 注册服务
     *
     * @return mixed
     */
    public function register()
    {
    	//
        $this->app->bind('userService',User::class);
    }


    /**
     * 执行服务
     */
    public function boot()
    {
        //
    }
}

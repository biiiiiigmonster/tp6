<?php

namespace app\verify\validate;

use think\facade\Env;
use think\Validate;

class Index extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id' => 'number',
        'certificate' => 'require',
        'code' => 'require',
        'receiver' => 'require',
        'scene' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.number' => 'id必须是数字',
    ];

    /**
     * SendMsg场景验证
     * @return Index
     */
    public function sceneSend()
    {
        return $this->only(['receiver','scene']);
    }

    /**
     * SendMsg场景验证
     * @return Index
     */
    public function sceneSendInternationalMsg()
    {
        return $this->only(['receiver','scene']);
//            ->append('receiver','checkInternationalMobile');//使用自定义验证方法
    }

    /**
     * SendMsg场景验证
     * @return Index
     */
    public function sceneSendEmail()
    {
        return $this->only(['receiver','scene']);
    }

    /**
     * Check场景验证
     * @return Index
     */
    public function sceneCheck()
    {
        return $this->only(['certificate', 'code', 'scene']);
    }

    /**
     * 国际手机号格式验证
     * @param $value
     * @return bool|string
     */
    protected function checkInternationalMobile($value)
    {
        $regex = Env::get('regex.international_phone');
        return preg_match($regex,$value) ? true : '手机号格式不正确';
    }
}
<?php

namespace app\verify\controller;

use think\exception\RouteNotFoundException;

class Error
{
    /**
     * 路由解析失败
     * @throws \Exception
     */
    public function miss()
    {
        throw new RouteNotFoundException();
    }
}

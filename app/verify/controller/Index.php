<?php

namespace app\verify\controller;

use app\BaseController;
use app\verify\biz\IndexBiz;
use app\verify\model\logic\IndexLogic;

class Index extends BaseController
{
    protected $middleware = [
        'Channel' => ['only' => ['send']],
    ];

    /**
     * @var IndexLogic
     */
    private $indexLogic;

    protected function initialize()
    {
        $this->indexLogic = new IndexLogic();
    }

    /**
     * 验证码发送
     * @return \think\response\Json
     * @throws \Exception
     */
    public function send()
    {
        $data = $this->request->param();

        //根据不同的账号类型自动识别调用信息发送渠道
        $result = $this->indexLogic->{$this->request->channel}($data);
        //数据返回
        return json($result);
    }
}

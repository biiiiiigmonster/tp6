<?php

use app\Request;
use app\verify\validate\Index;
use app\verify\middleware\Check;
use app\verify\middleware\Issue;
use think\exception\RouteNotFoundException;
use think\facade\Route;
use think\captcha\facade\Captcha;

Route::miss(function (){
    throw new RouteNotFoundException();
});

//验证码创建路由组
Route::group(function (){
    //显示图片验证码
    Route::get('/show',function (){
        return Captcha::create('only_nums');
    });//没有validate表示该接口没有对参数进行验证，一般接口不需要参数的时候，也就不会配置验证
    /**
     * 发送验证码，会自动根据账号类型来定义发送渠道，详细渠道配置见Channel中间件中的定义
     */
    Route::post('/send','Index/send')->validate(Index::class,'send')->middleware('Throttle','1,1');
})->middleware(Issue::class);

//验证码校验路由组
Route::group(function (){
    Route::post('/check',function (Request $request){
        return $request->verify_check;
    });
})->middleware(Check::class);

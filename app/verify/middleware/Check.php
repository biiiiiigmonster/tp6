<?php


namespace app\verify\middleware;


use Closure;
use think\facade\Log;
use think\Response;

class Check
{
    public function handle($request, Closure $next):Response
    {
        return $next($request);
    }
}
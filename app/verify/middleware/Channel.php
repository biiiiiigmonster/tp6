<?php
namespace app\verify\middleware;

use Closure;
use think\facade\Env;
use think\facade\Validate;
use think\Response;

class Channel
{
    public function handle($request, Closure $next):Response
    {
        $receiver = $request->only(['receiver']);

        /**
         * 这个地方通过接收者格式来指定信息发送渠道
         */
        switch ($receiver) {
            //短信（国内号）发送渠道
            case Validate::rule('receiver','mobile')->check($receiver):
                $request->channel = 'sendMsg';
                break;
            //短信（国际号）发送渠道
            case Validate::rule([
                'receiver' => function($value){
                    $regex = Env::get('regex.international_phone');
                    return preg_match($regex,$value)?true:false;
                }
            ])->check($receiver):
                $request->channel = 'sendInternationalMsg';
                break;
            //邮件发送渠道
            case Validate::rule('receiver','email')->check($receiver):
                $request->channel = 'sendEmail';
                break;
            default:
                $code = 400;
                return json(config('code')[$code])->code($code);
        }

        return $next($request);
    }
}
<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace app;

use app\common\exception\ApiException;
use app\common\exception\BizException;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\RouteNotFoundException;
use think\exception\ValidateException;
use think\facade\Log;
use think\helper\Arr;
use think\Response;
use Throwable;

/**
 * 应用异常处理类
 */
class ExceptionHandle extends Handle
{
    /**
     * 不需要记录信息（日志）的异常类列表
     * @var array
     */
    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];

    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param  Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void
    {
        // 使用内置的方式记录异常日志
        parent::report($exception);
    }

    /**
     * 特殊改动，  我这里要忽略所有的异常系统记录，该为自己处理
     * 免得一个错误自己记录一次，框架又记录一次
     * @param Throwable $exception
     * @return bool
     */
    protected function isIgnoreReport(Throwable $exception): bool
    {
        return true;
    }

    /**
     * Render an exception into an HTTP response.
     * 这里感觉有个很不合理的设计，此处处理了抛出异常之后，应该是直接响应至客户端
     * 而非又经过一次后置中间件的处理
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        // 添加自定义异常处理机制
        if($e instanceof RouteNotFoundException){
            $code = 404;
        }else if($e instanceof ValidateException){
            Log::record(sprintf(' %s At %s line %d', $e->getMessage(), $e->getFile(), $e->getLine()),'error');
            $code = 403;
        }else if($e instanceof BizException){
            $code = 200;
            $data = format(null,$e->getBizCode(),$e->getMessage());
        }else if($e instanceof ModelNotFoundException){
            $code = 200;
            $data = format(null,NULL_DATA);
        }else if($e instanceof ApiException){
            Log::record(sprintf(' %s At %s line %d', $e->getMessage(), $e->getFile(), $e->getLine()),'error');
            $code = 500;
            $data = $e->getMessage();
        }else{
            $code = 500;
            Log::record(sprintf(' %s At %s line %d', $e->getMessage(), $e->getFile(), $e->getLine()),'error');
        }

        $data = isset($data) ? $data : config('code')[$code];
        if(Arr::accessible($data)) {
            return json($data);
        }
        return response($data)->code($code);

        // 其他错误交给系统处理
//        return parent::render($request, $e);
    }
}

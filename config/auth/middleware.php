<?php

return [
    //接口形式相关中间件
    'alias' => [
        //token认证中间件
        'Authenticate'    => app\http\middleware\Authenticate::class,//负责验证用户授权信息
        //验证签名中间件
        'Signature'    => app\http\middleware\Signature::class,//这个验证比较严格，一般小项目不要轻易使用
        //请求频率限制中间件
        'Throttle'    => app\http\middleware\Throttle::class,//这个验证比较严格，一般小项目不要轻易使用
        //返回结果包装中间件
        'Launcher'    => app\http\middleware\Launcher::class,//响应结果发射器，主要用于统一格式
        //token授权中间件
        'Authorize'    => app\auth\middleware\Authorize::class,//负责生成用户授权标识
    ],
];

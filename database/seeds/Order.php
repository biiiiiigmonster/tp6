<?php

use think\migration\Seeder;

class Order extends Seeder
{
    protected $push = [];

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $data = [];
        for ($i = 1; $i <= 130; $i++) {
            $order_amount = $faker->randomFloat(2,0,499.99);
            $discount_amount = $faker->randomFloat(2,0,bcdiv($order_amount,$faker->numberBetween(2,9),2));
            $create_time = $faker->dateTimeBetween('2019-10-01 00:00:00','2019-10-31 23:59:59')->format('Y-m-d H:i:s');
            $status = $faker->numberBetween(1,5);
            $arr = [
                'user_id'       => $faker->numberBetween(1,30000),
                'merchant_id'       => $faker->numberBetween(1,30000),
                'trade_no'      => $faker->isbn10 . $faker->ean13,
                'out_trade_no'      => $faker->ean13 . $faker->isbn13,
                'order_amount' => $order_amount,
                'discount_amount'         => $discount_amount,
                'final_amount'    => $order_amount - $discount_amount,
                'status'            => $status,
                'pay_time'          => null,
                'create_time'       => $create_time,
                'update_time'       => date('Y-m-d H:i:s'),
            ];
            if($status>1) {
                $arr['pay_time'] = $faker->dateTimeBetween($create_time,date('Y-m-d H:i:s',strtotime($create_time.'+'.$faker->numberBetween(0,9).' minute +'.$faker->numberBetween(1,59).' second')))->format('Y-m-d H:i:s');
            }
            $data[] = $arr;
//            $this->push[] = $arr;
//            if(!($i%5)) $this->pushJob();
        }

        $this->table('order')->insert($data)->save();
        \think\facade\Db::connect('mysql1')->table('order')->insertAll($data);
        \think\facade\Db::connect('mysql2')->table('order')->insertAll($data);
        \think\facade\Db::connect('mysql3')->table('order')->insertAll($data);
    }

    protected function pushJob()
    {
        //这个就是消息队列的使用方式，希望以后能在实战中引用
        \think\facade\Queue::push('app\job\Copy',$this->push);
        $this->push = [];
    }
}
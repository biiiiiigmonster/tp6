<?php

use think\migration\Seeder;

class User extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run1()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $data = [];
        for ($i = 0; $i < 1525; $i++) {
            $arr = [
                'mobile'       => $faker->phoneNumber,
                'password'      => md5('abc12345'),
                'create_time'   => date('Y-m-d H:i:s'),
            ];
            $data[$arr['mobile']] = $arr;
        }

        $this->table('user')->insert(array_values($data))->save();
    }
}